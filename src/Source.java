import java.util.Locale;
import java.util.Scanner;

public class Source {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
		int z = scanner.nextInt();
		while (z-- > 0) {
			int numberOfNodes = scanner.nextInt() + 2;
			double[] x = new double[numberOfNodes];
			double[] y = new double[numberOfNodes];
			double[][] distances = new double[numberOfNodes][numberOfNodes];
			for (int j = 1; j < numberOfNodes; j++) {
				x[j] = scanner.nextDouble();
				y[j] = scanner.nextDouble();
			}
			calcDistances(distances, x, y, numberOfNodes);
			System.out.println(calcShortestRoute(distances, numberOfNodes));
		}
	}

	private static String calcShortestRoute(double[][] distances, int numberOfNodes) {
		double[][] shortestPathLength = new double[numberOfNodes][numberOfNodes];
		int[][] shortestPath = new int[numberOfNodes][numberOfNodes];
		shortestPathLength[0][0] = 0;
		shortestPathLength[0][1] = distances[0][1];
		for (int j = 2; j < numberOfNodes; j++) {
			shortestPathLength[0][j] = shortestPathLength[0][j - 1] + distances[j - 1][j];
			shortestPath[0][j] = j - 1;
		}

		for (int i = 1; i < numberOfNodes; i++) {
			for (int j = i; j < numberOfNodes; j++) {
				shortestPathLength[i][j] = -1;
				double min = Double.MAX_VALUE;
				int minK = -1;
				if (i == j || i == j - 1) {
					for (int k = 0; k < i; k++) {
						double temp = shortestPathLength[k][i] + distances[k][j];
						if (temp < min) {
							minK = k;
							min = temp;
						}
					}
					shortestPathLength[i][j] = min;
					shortestPath[i][j] = minK;
				} else {
					shortestPathLength[i][j] = shortestPathLength[i][j - 1] + distances[j - 1][j];
					shortestPath[i][j] = j - 1;
				}
			}
		}
		int[] list = new int[numberOfNodes];
		recurSP(list, shortestPath, numberOfNodes - 1, numberOfNodes - 1);
		int index = 1;
		double route1 = distances[0][1];
		StringBuilder result = new StringBuilder("S");
		while (index != numberOfNodes - 1) {
			route1 += distances[index][list[index]];
			if (index == 0) {
				result.append('S');
			} else if (index == numberOfNodes - 1) {
				result.append('P');
			} else {
				result.append((char) ('a' + (index - 1)));
			}
			index = list[index];
		}
		result.append("P");
		double route2 = 0;
		StringBuilder tmp = new StringBuilder();
		index = 0;
		while (index != numberOfNodes - 1) {
			route2 += distances[index][list[index]];
			if (index == 0) {
				tmp.insert(0, 'S');
			} else if (index == numberOfNodes - 1) {
				tmp.insert(0, 'P');
			} else {
				tmp.insert(0, (char) ('a' + (index - 1)));
			}
			index = list[index];
		}
		result.append(tmp);
		return String.format(Locale.ENGLISH, "%.2f=%.2f+%.2f:%s", shortestPathLength[numberOfNodes - 1][numberOfNodes - 1], route1, route2, result.toString());
	}

	private static void recurSP(int[] list, int[][] shortestPath, int i, int j) {
		if (!(i == 0 && j == 1 || j == 0 && i == 1)) {
			if (i > j) {
				int tmp = i;
				i = j;
				j = tmp;
			}
			list[shortestPath[i][j]] = j;
			recurSP(list, shortestPath, shortestPath[i][j], i);
		}
	}

	private static void calcDistances(double[][] distances, double[] x, double[] y, int numberOfNodes) {
		for (int i = 0; i < numberOfNodes; i++) {
			for (int j = 0; j < numberOfNodes; j++) {
				distances[i][j] = Math.sqrt((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]));
			}
		}
	}
}
